# README #

### What is this repository for? ###

This repository is for my simple CRUD application. It is a simple movie database.

### How do I get set up? ###

* You will need to run the files from a server, otherwise it won't run.
* Any web based environment or web server should be able to handle it.

### Who do I talk to? ###

* Malik Browne (malik@beesdesign.net)
* If you have any questions please contact me!